# Linux Installer for Archives (update code)

This repository contains the code responsible for updating the [liar](https://framagit.org/grumpyf0x48/liar) software [file](https://framagit.org/grumpyf0x48/liar/-/blob/master/config/liar-software).
